﻿using System.Collections.Generic;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Services;
using Ninject;
using Ninject.Activation;
using Ninject.Modules;
using Ninject.Planning.Bindings;
using IKernel = FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface.IKernel;

namespace FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Module
{
	public class InjectionKernel : StandardKernel, IKernel
	{
		private readonly DataProvider _dataProvider = new DataProvider();
		private readonly InjectionService _injectionService;
		private readonly IInjectionConfiguration _configuration;

		public InjectionKernel(IEnumerable<INinjectModule> modules)
		{
			_injectionService = new InjectionService(_dataProvider);
			_configuration = _dataProvider.Configuration;
			Load(modules);
		}

		#region Implementation of IBindingRoot
		public override void AddBinding(IBinding binding)
		{
			_injectionService.Bind(binding.Service);
			base.AddBinding(binding);
		}

		public override void RemoveBinding(IBinding binding)
		{
			_injectionService.Unbind(binding.Service);
			base.RemoveBinding(binding);
		}
		#endregion

		#region Implementation of IResolutionRoot
		public override IEnumerable<object> Resolve(IRequest request)
		{
			_injectionService.Resolve(request.Service);
			return base.Resolve(request);
		}
		#endregion

		#region Implementation of IKernel
		public IInjectionConfiguration Configuration
		{
			get { return _configuration; }
		}
		#endregion
	}
}
