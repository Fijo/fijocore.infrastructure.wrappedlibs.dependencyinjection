using System.Collections.Generic;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Exceptions;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Module
{
	public class Configuration : IInjectionConfiguration
	{
		private readonly IDictionary<string, object> _configuration = new Dictionary<string, object>();

		public void Add(string key, object value)
		{
			_configuration[key] = value;
		}
		
		public T Get<T>(string key)
		{
			object value;
			if(!_configuration.TryGetValue(key, out value)) throw new InjectionConfigurationNotFoundException(key, typeof (T));
			if(!(value is T)) throw new InjectionConfigurationInvalidTypeException(key, value.GetType(), typeof (T));
			return (T) value;
		}
	}
}