using System;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Services;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Store;

namespace FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Module
{
	internal class DataProvider
	{
		internal readonly IInjectionConfiguration Configuration = new Configuration();
		internal readonly IInjectionStore<Type> BindingStore = new BindingStore();
		internal InjectionService InjectionService;
		internal BindingService BindingService;
		internal ResolveService ResolveService;
		internal ResolveWatcher ResolveWatcher;

		internal DataProvider()
		{
			Configuration.Add("ResolveWatcher.ProcessOnCount", 10000);
			Configuration.Add("ResolveWatcher.CriticalResolveIntervall", 1);
			Configuration.Add("ResolveWatcher.NonUniquePercentPart", 80);
		}
	}
}