using System;
using System.Collections.Generic;
using System.Linq;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Dtos;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Exceptions;

namespace FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Module
{
	public class ResolveWatcher
	{
		private readonly IList<ResolveLog> _resolveLogs = new List<ResolveLog>();
		private readonly int _processOnCount;
		private readonly int _criticalBlockResolveIntervall;
		private readonly int _nonUniquePart;

		internal ResolveWatcher(DataProvider dataProvider)
		{
			dataProvider.ResolveWatcher = this;
			_processOnCount = dataProvider.Configuration.Get<int>("ResolveWatcher.ProcessOnCount");
			_criticalBlockResolveIntervall = dataProvider.Configuration.Get<int>("ResolveWatcher.CriticalResolveIntervall") * _processOnCount;
			_nonUniquePart = _processOnCount - (dataProvider.Configuration.Get<int>("ResolveWatcher.NonUniquePercentPart") / 100 * _processOnCount);
		}

		public void Resolve(Type type)
		{
			var now = DateTime.Now.Ticks;
			LogResolve(type, now);
			CheckLog(now);
		}

		private void CheckLog(long now)
		{
			if(_resolveLogs.Count < _processOnCount) return;
			var isCriticalBlockResolveIntervall = GetBlockResolveIntervall(now) <= _criticalBlockResolveIntervall;
			var isCriticalNonUniqueCount = GetDistinctedTypes().Count() <= _nonUniquePart;
			if(isCriticalBlockResolveIntervall && isCriticalNonUniqueCount) HandleCriticalState();
		}

		private void HandleCriticalState()
		{
			throw new CriticalInjectionStateException(GetDistinctedTypes());
		}

		private IEnumerable<Type> GetDistinctedTypes()
		{
			return _resolveLogs.Select(x => x.Type).Distinct();
		}

		private long GetBlockResolveIntervall(long now)
		{
			return _resolveLogs.First().Time - now;
		}

		private void LogResolve(Type type, long now)
		{
			_resolveLogs.Add(new ResolveLog(now, type));
		}
	}
}