using System;
using System.Collections.Generic;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Store
{
	public class BindingStore : IInjectionStore<Type>
	{
		private readonly IList<Type> _bindings = new List<Type>();

		#region Implementation of IStore<Type>
		public IEnumerable<Type> Get()
		{
			return _bindings;
		}

		public void Store(Type entity)
		{
			_bindings.Add(entity);
		}

		public void Remove(Type entity)
		{
			_bindings.Remove(entity);
		}
		#endregion
	}
}