using System;

namespace FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Exceptions {
	[Serializable]
	public class InjectionConfigurationException : Exception {
		public InjectionConfigurationException(string message) : base(message) {}
	}
}