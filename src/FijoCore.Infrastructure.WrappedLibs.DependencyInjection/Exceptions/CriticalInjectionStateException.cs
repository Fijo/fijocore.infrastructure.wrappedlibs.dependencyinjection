using System;
using System.Collections.Generic;
using System.Linq;

namespace FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Exceptions {
	[Serializable]
	public class CriticalInjectionStateException : Exception {
		public CriticalInjectionStateException(IEnumerable<Type> types) : base(string.Format("A critical usage of Injection Resolvings had been detected. This is may caused by a �resolving circle�. This exception may be thrown before a StackOverflowException. The last resolved types are {0}.", string.Join(", ", types.Select(x => x.Name)))) {}
	}
}