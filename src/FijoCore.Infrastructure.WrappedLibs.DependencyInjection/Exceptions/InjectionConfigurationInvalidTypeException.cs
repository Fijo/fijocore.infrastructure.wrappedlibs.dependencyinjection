using System;

namespace FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Exceptions
{
	[Serializable]
	public class InjectionConfigurationInvalidTypeException : InjectionConfigurationException
	{
		public InjectionConfigurationInvalidTypeException(string key, Type wrongType, Type wantedType) : base(string.Format("wrong value type {0} of configuration for key �{1}� detected. Please use a value of type {2}.", wrongType.Name, key, wantedType.Name)){}
	}
}