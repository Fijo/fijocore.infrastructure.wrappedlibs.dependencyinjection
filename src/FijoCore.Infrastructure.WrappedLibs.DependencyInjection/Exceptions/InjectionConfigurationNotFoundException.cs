using System;

namespace FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Exceptions
{
	[Serializable]
	public class InjectionConfigurationNotFoundException : InjectionConfigurationException
	{
		public InjectionConfigurationNotFoundException(string key, Type type) : base(string.Format("missing configuration for key �{0}� with value of type {1}", key, type.Name)){}
	}
}