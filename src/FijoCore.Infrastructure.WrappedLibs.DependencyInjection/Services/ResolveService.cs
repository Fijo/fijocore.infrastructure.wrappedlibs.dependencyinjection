using System;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Module;

namespace FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Services
{
	public class ResolveService
	{
		private readonly ResolveWatcher _resolveWatcher;

		internal ResolveService(DataProvider dataProvider)
		{
			dataProvider.ResolveService = this;
			_resolveWatcher = dataProvider.ResolveWatcher = new ResolveWatcher(dataProvider);
		}

		public void Resolve(Type type)
		{
		}
	}
}