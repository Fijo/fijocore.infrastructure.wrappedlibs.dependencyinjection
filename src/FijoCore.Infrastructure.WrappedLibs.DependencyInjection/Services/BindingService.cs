using System;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Module;

namespace FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Services
{
	public class BindingService
	{
		private readonly IInjectionStore<Type> _bindingStore;

		internal BindingService(DataProvider dataProvider)
		{
			dataProvider.BindingService = this;
			_bindingStore = dataProvider.BindingStore;
		}

		public void Bind(Type type)
		{
			_bindingStore.Store(type);
		}

		public void Unbind(Type type)
		{
			_bindingStore.Remove(type);
		}	
	}
}