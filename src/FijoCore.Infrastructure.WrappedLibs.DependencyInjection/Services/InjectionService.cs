using System;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Module;

namespace FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Services
{
	public class InjectionService
	{
		private readonly BindingService _bindingServices;
		private readonly ResolveService _resolverService;

		internal InjectionService(DataProvider dataProvider)
		{
			dataProvider.InjectionService = this;
			_bindingServices = new BindingService(dataProvider);
			_resolverService = new ResolveService(dataProvider);
		}

		public void Bind(Type type)
		{
			_bindingServices.Bind(type);
		}

		public void Unbind(Type type)
		{
			_bindingServices.Unbind(type);
		}

		public void Resolve(Type type)
		{
			_resolverService.Resolve(type);
		}
	}
}