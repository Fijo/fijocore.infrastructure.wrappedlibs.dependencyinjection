using System;

namespace FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Dtos
{
	public class ResolveLog
	{
		public readonly long Time;
		public readonly Type Type;

		public ResolveLog(long time, Type type)
		{
			Time = time;
			Type = type;
		}
	}
}