namespace FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface
{
	public interface IInjectionConfiguration
	{
		void Add(string key, object value);
		T Get<T>(string key);
	}
}