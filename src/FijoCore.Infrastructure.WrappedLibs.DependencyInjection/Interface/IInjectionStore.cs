using System.Collections.Generic;

namespace FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface
{
	public interface IInjectionStore<TEntity>
	{
		IEnumerable<TEntity> Get();
		void Store(TEntity entity);
		void Remove(TEntity entity);
	}
}