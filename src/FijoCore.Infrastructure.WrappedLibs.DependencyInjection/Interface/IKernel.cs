namespace FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface
{
	public interface IKernel : Ninject.IKernel
	{
		IInjectionConfiguration Configuration { get; }
	}
}